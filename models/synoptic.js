'use strict';
const mongoose = require('mongoose');

const Schema = mongoose.Schema;
const ObjectId = Schema.ObjectId;

const VARIABLE = {
  name: String,
  class: String,
  device: String,
};

let synopticSchema = mongoose.Schema({
  _id: {
    type: ObjectId,
    auto: true,
  },
  name: {
    type: String,
    required: true,
  },
  user: {
    type: String,
    required: true,
  },
  svg: {
    type: String,
    required: true,
  },
  variables: {
    type: [VARIABLE],
    required: false,
    default: [],
  },
  insertTime: {
    type: Date,
    default: Date.now,
  },
  updateTime: {
    type: Date,
    required: true,
    default: Date.now,
  },
  group: {
    type: String,
    required: false,
    default: null,
  },
  groupWriteAccess: {
    type: Boolean,
    required: false,
    default: false,
  },
  lastUpdatedBy: {
    type: String,
    required: false,
    default: null,
  },
  deleted: {
    type: Boolean,
    default: false,
  },
  tangoDB: {
    type: String,
    default: '',
  },
});

module.exports = mongoose.model('Synoptic', synopticSchema);
